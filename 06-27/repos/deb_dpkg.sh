#!/bin/bash
source func_dpkg
dep="/var/cache/apt/archives/*.deb"

#Descargar los ficheros .deb de un programa
read -p "Por favor ingresar el programa que desea descargar: " program
                deb_check
        echo "Se esta descargando $program.."
        sudo apt-get -qq install -d $program 2>/dev/null
        echo "La descarga ha sido completada."

#Procesar ficheros .deb
read -p "Le gustaría instalar el programa y todas sus dependencias? [y/n] " ANS
        if [[ $ANS =~ [yY] ]]; then
           for file in $dep; do
                sudo dpkg -i $file && echo "Se ha instalado $programa y sus dependencias."
           done
           sudo apt clean
        else
           echo "No se ha instalado $program."
        fi
