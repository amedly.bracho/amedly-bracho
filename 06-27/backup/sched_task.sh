#!/bin/bash

#Revisar si usuario es Root
root_chk () {
        if [[ $USER != "root" ]]; then
                echo "Se debe ejecutar el script de la siguiente manera: sudo $0"
                exit 1
        fi
}


#Crear fichero cron y configurar task
task_conf () {
local script="/home/$USER/amedly-bracho/06-27/backup/key.sh"
local USER_CRON="/var/spool/cron/crontabs/$USER"

        if [[ ! -f $USER_CRON ]]; then
        touch $USER_CRON
        fi

        echo "* 20 * * 1-5 $script" >> $USER_CRON
        echo "Fichero cron para usuario $USER ha sido configurado de la siguiente forma:"
        cat $USER_CRON
}


root_chk
task_conf
